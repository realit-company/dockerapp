# Dockerapp

This is a much more than a simple repository !

## Prerequisites

* vim must be installed
* docker must be installed and started
* docker-compose must be installed

## Usage

1. Clone the repo

2. Navigate inside /repo directory

3. run  ```sudo ./dockerapp-tool [install/remove] [service-name]```